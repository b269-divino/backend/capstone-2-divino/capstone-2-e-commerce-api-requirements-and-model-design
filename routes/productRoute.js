const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");

// Create Product (Admin only)

router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});




// router.post("/create", auth.verify, (req, res) => {

// 	const data = {
// 		product: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}
// 	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
// });

// Retrieve all active products

router.get("/allActive" , (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Retrieve all products
router.get("/all" , (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// // // Retrieve single product

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a course
router.put("/:productId", auth.verify, (req,res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// // Route to archiving a product
router.patch("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;