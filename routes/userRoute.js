const express = require("express");
// Create a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving User details
router.get("/:userId", (req, res) => {
	userController.getUser(req.params).then(resultFromController => res.send(resultFromController));
});



// Route for retrieving all the User details
router.put("/all", (req, res) => {
	userController.getAllUser().then(resultFromController => res.send(resultFromController));
});





// //route for authenticated user enrollment
// router.post("/cart", auth.verify, (req, res) => {
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.courseId,
// 		quantity: req.body.quantity
// 	}
// 	userController.cart(data).then(resultFromController => res.send(resultFromController));
// });

router.post("/cart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	userController.cart(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;







