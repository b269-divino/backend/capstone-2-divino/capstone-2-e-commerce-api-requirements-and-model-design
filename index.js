// Set up dependencies
const express = require("express");
const mongoose = require ("mongoose");
const cors = require("cors");

// This allows us to use all the routed defined in "userRoute.js"
const userRoute = require("./routes/userRoute");
// This allows us to use all the routed defined in "productRoute.js"


const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all the user routed created in "userRoute.js" file to use "/users" as route (resources)
// localhost:5000/users
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

// Database Connection
mongoose.connect("mongodb+srv://joeydivino:admin123@zuitt-bootcamp.tkfjv2z.mongodb.net/capstoneBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true ,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log(`We can do this! We are now connected to cloud database!`));

// Server listening
// Will used the defined port number for the application whenever environment variable is available to used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 5000, () => console.log(`We can do this! Now connected to port ${process.env.PORT || 5000}`));





