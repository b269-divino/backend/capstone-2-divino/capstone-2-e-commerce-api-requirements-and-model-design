// Order Data Model Design

const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId : {
		type : String,
		required : [true, "User Id is required"]
	},
	totalAmount : {
		type : Number,
		required : [true, "Total Amount is required"]
	},
	// isActive : {
	// 	type : Boolean,
	// 	default : true
	// },
	purchaseOn : {
		type : Date, 
		default : new Date()
	},
	products : [
		{
			productId : {
				type : String,
				required : [true, "Your Product ID is required"]
			},
			quantity : {
				type : Number,
				required : [true, "Quantity is required"]
			}
		}
	]
});

module.exports = mongoose.model("Order", orderSchema);

