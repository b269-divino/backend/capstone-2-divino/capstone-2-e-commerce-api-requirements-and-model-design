// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");

const Product = require("../models/Product");

const Order = require("../models/Order");

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
const bcrypt = require("bcrypt");

const auth = require("../auth");



// User Registration

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// User Authentication


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		// User does not exist
		if (result == null){
			return false
		  // User exists
		} else {
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the passwords match/result of the above code is true
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			  // If password do not match
			} else {
				return false;
			};
		};
	});
};


// Route for retrieving User details

module.exports.getAllUser = () => {
	return User.find().then(result => {
		result.password = "";
		return result;
	});
};


// // Route for retrieving all the User details
module.exports.getUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		return result;
	});
};



// let isUserUpdated = await User.findById(data.userId).then(async (user) => {
// 		const product = await Product.findById(data.productId); // Retrieve the product object from the database
// 		const items = {
// 			productId: data.productId,
// 			Price: data.quantity,
// 			quantity: data.quantity,
// 			purchasedOn: new Date(),
// 			totalAmount: product.price * data.quantity, // Calculate the total amount based on the product price and the quantity




module.exports.cart = async (data) => {

let isUserUpdated = await User.findById(data.userId).then(async (user) => {
		const product = await Product.findById(data.productId);

        let totalAmount = 0;

        const items = {
            productId: data.productId,
            productName: product.name,
            quantity: data.quantity,
            purchasedOn: new Date(),
            subtotal: data.quantity * product.price,
            price: product.price,
        };


        user.purchases.push(items);

        const finalAmount = user.purchases.reduce(
      (total, item) => total + item.subtotal,
      0
    );

    user.totalAmount = finalAmount;

		return user.save().then((user, error) => {
			if (error){
				return true;
			} else {
				return false;
			};
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.Orders.push({userId: data.userId});
		return product.save().then((product, error) => {
			if (error){
				return true;
			} else {
				return false;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return false;
	} else {
		return true;
	};
};
