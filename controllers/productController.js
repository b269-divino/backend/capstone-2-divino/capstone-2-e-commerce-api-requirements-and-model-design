const Product = require("../models/Product");

// Create Product (Admin only)

module.exports.addProduct = (data) => {

	if (data.isAdmin) {

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {

			if (error) {
				return false;

			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};

// Retrieve all products

module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	});
};






// Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	});
};


// // Retrieve single product

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Update a course
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		};
	});
};


// Archive a product

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


