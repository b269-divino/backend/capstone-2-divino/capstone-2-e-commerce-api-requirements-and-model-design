const User = require("../models/User");

const Product = require("../models/Product");

const Order = require("../models/Order");

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
const bcrypt = require("bcrypt");

const auth = require("../auth");


